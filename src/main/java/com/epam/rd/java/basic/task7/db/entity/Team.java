package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class Team {

	public static Team createTeam(String name) {
		return new Team(name);
	}

	private int id;
	private String name;

	private Team(){
	}

	private Team(String name){
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		Team other = (Team) obj;
		return Objects.equals(this.name, other.name);
	}

}
