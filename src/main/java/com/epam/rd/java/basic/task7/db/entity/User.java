package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class User {

	public static User createUser(String login) {
		return new User(login);
	}

	private int id;
	private String login;

	private User(){
	}

	private User(String login){
		this.login = login;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Override
	public String toString() {
		return login;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		User other = (User) obj;
		return Objects.equals(this.login, other.login);
	}
}
