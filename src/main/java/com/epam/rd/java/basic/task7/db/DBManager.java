package com.epam.rd.java.basic.task7.db;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;
import org.apache.ibatis.jdbc.ScriptRunner;


public class DBManager {

	private static DBManager instance;
	private final String FULL_URL;

	private final String SELECT_ALL_USERS = "SELECT * FROM users;";
	private final String SELECT_ALL_TEAMS = "SELECT * FROM teams;";
	private final String ALTER_USERS_TEAMS_USER_ID_TO_FK = "ALTER TABLE users_teams ADD CONSTRAINT users_fk FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE;";
	private final String ALTER_USERS_TEAMS_TEAM_ID_TO_FK = "ALTER TABLE users_teams ADD CONSTRAINT teams_fk FOREIGN KEY(team_id) REFERENCES teams(id) ON DELETE CASCADE;";

	private final String SELECT_USER_BY_LOGIN = "SELECT * FROM users WHERE login = ?;";
	private final String SELECT_TEAM_BY_LOGIN = "SELECT * FROM teams WHERE  name = ?;";
	private final String SELECT_TEAMS_BY_USER = "SELECT * FROM teams JOIN users_teams ON teams.id = users_teams.team_id WHERE users_teams.user_id = ?;";
	private final String INSERT_USER_BY_LOGIN = "INSERT users(login) VALUES (?);";
	private final String INSERT_TEAM_BY_LOGIN = "INSERT teams(name)  VALUES (?);";
	private final String INSERT_USERS_TEAMS_BY_IDS = "INSERT users_teams(user_id, team_id) VALUE (?, ?);";
	private final String DELETE_USER_BY_ID = "DELETE FROM users WHERE id = ?;";
	private final String DELETE_TEAM_BY_ID = "DELETE FROM teams WHERE id = ?;";
	private final String UPDATE_TEAM_BY_ID = "UPDATE teams SET name = ? WHERE id = ?;";

	private static void dbCreate() {
		try (Connection conn = DriverManager.getConnection(instance.FULL_URL)){
			ScriptRunner sr = new ScriptRunner(conn);
			try(Reader reader = new BufferedReader(new FileReader("sql\\db-create.sql"));){
				sr.runScript(reader);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
	}

	public static synchronized DBManager getInstance() {
		if(instance == null){
			instance = new DBManager();
			//dbCreate();

			try(Connection conn = DriverManager.getConnection(instance.FULL_URL);
				Statement stmt = conn.createStatement()) {
				stmt.executeUpdate(getInstance().ALTER_USERS_TEAMS_USER_ID_TO_FK);
				stmt.executeUpdate(getInstance().ALTER_USERS_TEAMS_TEAM_ID_TO_FK);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return instance;
	}

	private DBManager() {
		Properties props = new Properties();
		try(InputStream in = Files.newInputStream(Paths.get("app.properties"))){ props.load(in); }
		catch (IOException e) { e.printStackTrace(); }
		FULL_URL = props.getProperty("connection.url");
	}

	public List<User> findAllUsers() {
		List<User> users = null;

		try(Connection conn = DriverManager.getConnection(FULL_URL);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(SELECT_ALL_USERS)){

			users = new ArrayList<>();

			while (rs.next()) {
				User user = User.createUser(rs.getString("login"));
				user.setId(rs.getInt("id"));
				users.add(user);
			}
		}
		catch (SQLException e){
			e.printStackTrace();
			users = null;
		}

		return users;
	}

	public boolean insertUser(User user) throws DBException {
		try(Connection conn = DriverManager.getConnection(FULL_URL);
			PreparedStatement prepStmt = conn.prepareStatement(INSERT_USER_BY_LOGIN, Statement.RETURN_GENERATED_KEYS)) {
			prepStmt.setString(1, user.getLogin());
			prepStmt.executeUpdate();

			ResultSet generatedKeys = prepStmt.getGeneratedKeys();

			if(generatedKeys.next()){
				int id = generatedKeys.getInt(1);
				user.setId(id);
			}

			return true;
		} catch (SQLException exception) {
			exception.printStackTrace();
		}
		return false;
	}

	public boolean deleteUsers(User... users) throws DBException {
		if(users.length == 0) return false;

//		try(Connection conn = DriverManager.getConnection(instance.FULL_URL);
//			Statement stmt = conn.createStatement()) {
//			stmt.executeUpdate(getInstance().ALTER_USERS_TEAMS_USER_ID_TO_FK);
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}

		try(Connection conn = DriverManager.getConnection(FULL_URL);
			PreparedStatement prepStmt = conn.prepareStatement(DELETE_USER_BY_ID)) {
			for(User user : users){
				prepStmt.setInt(1, user.getId());
				prepStmt.executeUpdate();
			}

			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public User getUser(String login) throws DBException {
		User user = null;

		try(Connection conn = DriverManager.getConnection(FULL_URL);
			PreparedStatement prepStmt = conn.prepareStatement(SELECT_USER_BY_LOGIN)) {
			prepStmt.setString(1, login);
			try(ResultSet rs = prepStmt.executeQuery()) {

				if (rs.next()) {
					user = User.createUser(rs.getString("login"));
					user.setId(rs.getInt("id"));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			user = null;
		}

		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = null;

		try(Connection conn = DriverManager.getConnection(FULL_URL);
			PreparedStatement prepStmt = conn.prepareStatement(SELECT_TEAM_BY_LOGIN)) {
			prepStmt.setString(1, name);
			try(ResultSet rs = prepStmt.executeQuery()) {

				if (rs.next()) {
					team = Team.createTeam(rs.getString("name"));
					team.setId(rs.getInt("id"));
				}
			}
		} catch (SQLException e) {
			team = null;
			e.printStackTrace();
		}

		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = null;

		try(Connection conn = DriverManager.getConnection(FULL_URL);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(SELECT_ALL_TEAMS)) {
			teams = new ArrayList<>();

			while(rs.next()){
				Team team = Team.createTeam(rs.getString("name"));
				team.setId(rs.getInt("id"));
				teams.add(team);
			}
		} catch (SQLException e) {
			teams = null;
			e.printStackTrace();
		}

		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		try(Connection conn = DriverManager.getConnection(FULL_URL);
			PreparedStatement prepStmt = conn.prepareStatement(INSERT_TEAM_BY_LOGIN, Statement.RETURN_GENERATED_KEYS)) {
			prepStmt.setString(1, team.getName());
			prepStmt.executeUpdate();

			ResultSet generatedKeys = prepStmt.getGeneratedKeys();

			if(generatedKeys.next()){
				int id = generatedKeys.getInt(1);
				team.setId(id);
			}

			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		if(teams.length == 0) return false;

		try(Connection conn = DriverManager.getConnection(FULL_URL)) {
			conn.setAutoCommit(false);
			try(PreparedStatement prepStmt = conn.prepareStatement(INSERT_USERS_TEAMS_BY_IDS)){
				for(Team team: teams){
					prepStmt.setInt(1, user.getId());
					prepStmt.setInt(2, team.getId());
					prepStmt.executeUpdate();
				}
				conn.commit();

				return true;
			}
			catch(SQLException e){
				conn.rollback();
				e.printStackTrace();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = null;

		try(Connection conn = DriverManager.getConnection(FULL_URL);
			PreparedStatement prepStmt = conn.prepareStatement(SELECT_TEAMS_BY_USER)) {

			prepStmt.setInt(1, user.getId());
			try(ResultSet rs = prepStmt.executeQuery()){
				teams = new ArrayList<>();

				while(rs.next()){
					Team team = Team.createTeam(rs.getString("name"));
					team.setId(rs.getInt("id"));

					teams.add(team);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			teams = null;
		}

		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
/*		try(Connection conn = DriverManager.getConnection(instance.FULL_URL);
			Statement stmt = conn.createStatement()) {
			stmt.executeUpdate(getInstance().ALTER_USERS_TEAMS_TEAM_ID_TO_FK);
		} catch (SQLException e) {
			e.printStackTrace();
		}*/

		try(Connection conn = DriverManager.getConnection(FULL_URL);
			PreparedStatement prepStmt = conn.prepareStatement(DELETE_TEAM_BY_ID)) {
			prepStmt.setInt(1, team.getId());
			prepStmt.executeUpdate();

			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public boolean updateTeam(Team team) throws DBException {
		try(Connection conn = DriverManager.getConnection(FULL_URL);
			PreparedStatement prepStmt = conn.prepareStatement(UPDATE_TEAM_BY_ID)) {
			prepStmt.setString(1, team.getName());
			prepStmt.setInt(2, team.getId());
			prepStmt.executeUpdate();

			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

}
